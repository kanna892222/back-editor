const path = require('path')
const resolve = dir => path.join(__dirname, dir)
// const CompressionPlugin = require('compression-webpack-plugin')

module.exports = {
  chainWebpack: config => {
    // 添加别名
    config.plugins.delete('prefetch')
    // config.plugin('CompressionPlugin').use(CompressionPlugin)
    config.resolve.alias
      .set('@', resolve('src'))
      // .set('@assets', resolve('src/assets'))
      .set('@scss', resolve('src/assets/scss'))
      .set('@api', resolve('src/config/api'))
      .set('@utils', resolve('src/utils'))
      .set('@components', resolve('src/components'))
    // .set('vue$', 'vue/dist/vue.esm.js')
    // .set('@plugins', resolve('src/plugins'))
    // .set('@views', resolve('src/views'))
    // .set('@router', resolve('src/router'))
    // .set('@store', resolve('src/store'))
    // .set('@layouts', resolve('src/layouts'))
    // .set('@static', resolve('src/static'));
  },
  configureWebpack: {
    performance: {
      maxEntrypointSize: 512000,
      maxAssetSize: 512000
    }
  }
}
